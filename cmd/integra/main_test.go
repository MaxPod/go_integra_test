// +build integration

 package main
 
 import (
	 "testing"
	 "net/http"
	 "net/http/httptest"
	 "github.com/stretchr/testify/assert"
	 "fmt"
	 )


 var _ = func() bool {
		testing.Init()
		return true
 }()


 type testCase struct{
	 name string
	 jsonRequest string
	 wantResponce []byte
 } 

 func TestHandleCalculation(t *testing.T) {
	testCases := []testCase{
		{
			name : "Get",
			jsonRequest : 	`/`,
			wantResponce : []byte (`Welcome to the home page!`),
		},
	}
	
	handler := http.HandlerFunc(ShowWelcome)
	for _, tc := range testCases {
		t.Run (tc.name, func (t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", fmt.Sprintf(tc.jsonRequest), nil)
			handler.ServeHTTP(rec, req)
			assert.Equal(t, tc.wantResponce, rec.Body.Bytes())
		})
	}


}
 