package main

import (
	"flag"
	"fmt"
	"net/http"
	
	
)

type apiHandler struct{}


func (apiHandler) ServeHTTP(http.ResponseWriter, *http.Request) {}


func main() {
	testMode := flag.Bool("test", false, "режим интеграционного теста")
	flag.Parse()

	if *testMode {
		fmt.Println(" Run in test mode")
	} else {
		fmt.Println(" Run in work mode")	
	}

	mux := http.NewServeMux()
	mux.Handle("/api/", apiHandler{})
	mux.HandleFunc("/", ShowWelcome)
	err := http.ListenAndServe("localhost:9000", mux)

	if err != nil {
		fmt.Println(err)
	}
}

func ShowWelcome(w http.ResponseWriter, req *http.Request) {
	// "/" паттерн соотвествует всему, 
	// поэтому нам нужно проверить
	// что это на самом деле запрос корня
	if req.URL.Path != "/" {
	  http.NotFound(w, req)
	  return
	}
	fmt.Fprintf(w, "Welcome to the home page!")
  }